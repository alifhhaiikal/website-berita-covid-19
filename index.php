<?php
$url = file_get_contents('https://api.kawalcorona.com/indonesia');
$data = json_decode($url, true);
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello, world!</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" > 
    
  </head>
  <body>
    
  <nav class="navbar navbar-expand navbar-dark bg-dark">
      <div class="nav navbar-nav">
          <a class="nav-item nav-link active" href="#"><i class="fa fa-fw fa-home"></i> Home <span class="sr-only">(current)</span></a>
      </div>
  </nav>
    
    <div class="container">
      <div class="row mt-4">
        <div div class="col-md-12">
          <div class="alert alert-primary" role="alert">
              <strong>Update</strong> Data Virus Corona di Indonesia!
          </div>
        </div>  

        <div class="col-md-3">
          <div class="card badge-primary p-3 mb-5 shadow rounded">
            <div class="card-body">
              <h4 class="card-title text-center"><i class="fa fa-frown-open"></i> Positif</h4>
              <p class="card-text text-center"> <?= $data[0]['positif'] ?></p>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card badge-warning p-3 mb-5 shadow rounded">
            <div class="card-body">
              <h4 class="card-title text-center"><i class="fa fa-frown-open"></i> Dirawat</h4>
              <p class="card-text text-center"> <?= $data[0]['dirawat'] ?></p>
            </div> 
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="card badge-success p-3 mb-5 shadow rounded">
            <div class="card-body">
              <h4 class="card-title text-center"><i class="fa fa-grin"></i> Sembuh</h4>
              <p class="card-text text-center"> <?= $data[0]['sembuh'] ?></p>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card badge-danger p-3 mb-5 shadow rounded">
            <div class="card-body">
              <h4 class="card-title text-center"><i class="fa fa-frown-open"></i> Meninggal</h4>
              <p class="card-text text-center"> <?= $data[0]['meninggal'] ?></p>
            </div>
          </div>
        </div>

        <div class="row justify-content-center">
          <div class="col-4">
            <a href="https://kawalcovid19.id/" target="_blank" class="btn btn-primary">Selengkapnya &raquo;</a>
          </div>
        </div>
        
          <div class="card-footer bg-dark text-light fixed-bottom">
            <div class="container my-auto">
              Created By Me
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
   

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>

                        